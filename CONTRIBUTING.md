# Principal Investigator
Joshua Goh

# Postdoctoral Associate
Poyu Chen

# Graduate Students
- Yu-Shiang Su
- Jheng-Ting Chen
- Yong-Jheng Tang
- Shu-Yun Yuan
- Dong-Wei Lin
- Hsin-Yi Hung

# Undergraduate Students
- Yun-Shiuan Chuang

# Funding
1. Age-Related Effects on the Neural Correlates of Decision-Making Processing. Taiwan National Science Council Research Grant 102-2410-H-002-004-MY3, Jan 2013 to Oct 2015.
2. Biomolecular Contributions to Brain and Cognitive Aging. Ministry of Science and Technology Research Grant 103-2410-H-002-082-MY2, Aug 2014 to Jul 2016.
3. Neural Mechanisms and Individual Differences Underlying Aging of Value-Based Decision Processing. Taiwan Ministry of Science and Technology Grant 105-2420-H-002-MY2, Jan 2016 to Dec 2017.
4. Dopaminergic and serotonergic contributions to age-related effects on value-based decision processing. Taiwan Ministry of Science and Technology Grant 105-2410-H-002-055-MY3, Aug 2016 to Jul 2019.