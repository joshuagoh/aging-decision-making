# Aging Decision Making Project
Study on younger and older adults performing the Lottery Choice Task while undergoing functional magnetic resonance imaging or electroencephalography. This project was active for data collection between 2012 to 2015.

## Directory Overview
- info: Participant recruitment ads, videos on procedures @NCCU-MRI.
- experimental_protocol: E-Prime es2 files for the Lottery Choice Task (CN), modified from the BLSA version ([Goh et al., 2016](https://www.jneurosci.org/content/jneuro/36/49/12498.full.pdf)).
- docs: Consent forms, neuropsychological assessment forms, scan logs.

**Notes**
- Server copy includes #tables (not included in online repo).

## Manuscripts
1. [Su, Y. S., Chen, J. T., Tang, Y. J., Yuan, S. Y., McCarrey, A. C., Goh, J. O. S.* (2018). Age-Related Differences in Striatal, Medial Temporal, and Frontal Involvement During Value-Based Decision Processing. Neurobiology of Aging, 69:185-198.](https://pubmed.ncbi.nlm.nih.gov/29909176/)
2. [Chuang, Y. S., Su, Y. S., Goh, J. O. S.* (2020). Neural responses reveal associations between personal values and value-based decisions. Social, Cognitive, Affective Neuroscience. nsaa150, https://doi.org/10.1093/scan/nsaa150.](https://academic.oup.com/scan/article/15/11/1217/5956559?login=true)
3. [Chen, P. Y., Hung, H. Y., Goh, J. O. S.* (2023). Age-related differences in ERP correlates of value-based decision making. Neurobiology of Aging. 123, 10-22. doi: 10.1016/j.neurobiolaging.2022.11.008](https://www.sciencedirect.com/science/article/abs/pii/S0197458022002378)


## Conference Abstracts
1. [Su, Y.-S., Tai, T.-L., Goh, J. O. S. (2014). Age reduces striatal selectivity and increases frontal activation during choice value processing. Presented at the Society for Neuroscience Annual Meeting, Washington, DC, USA.](https://www.abstractsonline.com/Plan/ViewAbstract.aspx?sKey=dfb1940f-4788-4449-8a67-5df586bc95d9&cKey=3d58c6c6-d9a7-4ed6-934b-0ded8b057246&mKey=%7b54C85D94-6D69-4B09-AFAA-502C0E680CA7%7d)
2. Chuang, Y.-S., Goh, J. O. S. (2017). Personal values modulate risk-taking behaviors: Hedonism as an accelerator and security as a brake. Presented at Joint Conference of National Taiwan University, Peking University, and Chinese University of Hong Kong Psychology Departments, Beijing, China.
3. Hung, H.-Y., Chen, P., Goh, J. (2016). Age differences in mutual information of electroencephalograms during value-based decision-making. 648.14 / MMM39. Presented at the Society for Neuroscience Annual Meeting, San Diego, CA, USA.
4. [Chen, P., Hung, H.-Y., Goh, J. (2016). P3 like late positivity dissociates young and older adult value-based decision processing. 749.16 / LLL41. Presented at the Society for Neuroscience Annual Meeting, San Diego, CA, USA.](https://drive.google.com/file/d/0BzjcVTDEKxivRkVtbjIxVWcxZG8/view?resourcekey=0-tjfdaJMyQrjXMq3mtBISGw)
5. [Lin, D.-W., Goh, J. O. S. (2016). Neural correlates of emotional feedback influences on valuative decisions in young and older adults. Presented at the Organization for Human Brain Mapping, Geneva, Switzerland.](https://drive.google.com/file/d/0BzjcVTDEKxivQW5YT0JKSFJESmM/view?resourcekey=0-YqqvDpaLm4FdduWCLh1d0Q)
6. [Su, Y.-S., Tai, T.-L., Goh, J. O. S. (2014). Age reduces striatal selectivity and increases frontal activation during choice value processing. Presented at the Society for Neuroscience Annual Meeting, Washington, DC, USA.](https://www.abstractsonline.com/Plan/ViewAbstract.aspx?sKey=dfb1940f-4788-4449-8a67-5df586bc95d9&cKey=3d58c6c6-d9a7-4ed6-934b-0ded8b057246&mKey=%7b54C85D94-6D69-4B09-AFAA-502C0E680CA7%7d)

## Media
1. Different brain areas contribute to decision making in young and older adults. MOST Center for Global Affairs and Science Engagement (GASE) Newsletter, Taiwan. [https://trh.gase.most.ntnu.edu.tw/en/article/content/47](https://trh.gase.most.ntnu.edu.tw/en/article/content/47)

[Contributors](/CONTRIBUTING.md)

[Changelog](/CHANGELOG.md)